using System;
using System.Timers;

namespace blazorSvr.Utils{
    public class Tools{
        public static string getToday(){
            var day = DateTime.Now;
            return day.ToString();
        }
        public static string getDateOnlyNumerics(){
            var s = getToday();
            string[] wk = s.Split(" ");
            return wk[0].Replace("/", "");
        }

    }
}
