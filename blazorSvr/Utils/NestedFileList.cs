using System.IO;
using System.Collections.Generic;

namespace blazorSvr.Utils
{
    public class NestedFileList
    {
        public static DirInfo Get(string dirPath){
            DirInfo di = new DirInfo();
            di.RootDir = dirPath;
            di.DirPath = dirPath;
            return recurs(di, 1);
        }
        private static DirInfo recurs(DirInfo di, int nest){
            int wkNest = nest + 1;
            di.Files = Directory.GetFiles(di.DirPath);
            string[] dirs = Directory.GetDirectories(di.DirPath);
            foreach(string s in dirs){
                DirInfo _di = new DirInfo();
                _di.RootDir = di.RootDir;
                _di.DirPath = s;
                _di.Files = Directory.GetFiles(s);
                if(wkNest <= 2){
                    recurs(_di, wkNest);
                }
                di.SubDirs.Add(_di);
            }
            return di;
        }
    }

    public class DirInfo{
        public string RootDir{get; set;}
        public string DirPath{get; set;}
        public List<DirInfo> SubDirs = new List<DirInfo>();
        public string[] Files{get; set;}
    }

    static class DirInfoExtensions{
        public static List<string> getFileNameList(this DirInfo d){
            List<string> li = new List<string>();
            foreach(string s in d.Files){
                li.Add(Path.GetFileName(s));
            }
            return li;
        }

        public static List<string> getFolderList(this DirInfo d){
            List<string> li = new List<string>();
            foreach(DirInfo di in d.SubDirs){
                DirectoryInfo info = new DirectoryInfo(di.DirPath);
                li.Add(info.Name);
            }
            return li;
        }

        public static string getFoldername(this DirInfo d){
            DirectoryInfo info = new DirectoryInfo(d.DirPath);
            return info.Name;
        }

        public static string getFolderName(this DirInfo d){
            char pathSep = Path.DirectorySeparatorChar;
            var wkArr = d.DirPath.Split(pathSep);
            return wkArr[^1];
        }
    }

}