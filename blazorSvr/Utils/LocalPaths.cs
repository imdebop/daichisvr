using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace blazorSvr.Utils
{
    public class LocalPaths
    {
        static Dictionary<string, string> dicLocalPath; // = new Dictionary<string, string> {};
        
        static LocalPaths()
        {
            //Console.WriteLine("localizeCs.ProgramPath.gethPathDictionary called. ********");
            dicLocalPath = localizeCs.ProgramPath.getPathDictionary();
        }

        public static List<string> GetRecList(){
            List<string> wk = new List<string>();
            foreach(KeyValuePair<string, string> item in dicLocalPath){
                wk.Add(item.Key + "=" + item.Value);
            }
            return wk;
        }

        public static string GetPath(string folderName){
            string res = dicLocalPath[folderName];
            return res;
        }
    }
}