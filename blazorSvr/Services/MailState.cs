using System.ComponentModel.DataAnnotations;

namespace blazorSvr.Services{
    public class MailState{
        [Required(ErrorMessage = "概要を記入してください。↑")]
        public string Title_about{get; set;} = "";
        public string Title{get; set;}
        public string MailText{get; set;} = "";
        
        public string MailDataFolderName{get; set;}
        //<summary>path + filename</summary>
        //public string ZipFilePath{get; set;}

        public bool DbUpdate = true;
        public bool MailSendOk = true;

        public bool MailTest = true;

    }

    public static class MailStateExtentions{
        public static void MStateClear(this MailState ms){
            ms.Title = "";
            ms.Title_about = "";
            ms.MailText = "";
            ms.MailDataFolderName = "";
            //ms.ZipFilePath = "";
        }



    }

}