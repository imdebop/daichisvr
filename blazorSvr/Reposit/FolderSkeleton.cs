//using System;
using System.Linq;

namespace blazorSvr.Reposit{
    public class FolderSkeleton{
        public static readonly string[] subFolders = {
            "仮換地証明データ", 
            "個別重ね図Docu_公図入・名前無",
            "個別重ね図Docu_公図入・名前入",
            "個別重ね図Docu_公図無・名前無",
            "個別重ね図Docu_公図無・名前入",
            "仮換地状況調書"
        };

    }

    public static class FolderSkeletonExtentions{
        public static bool FolderExists(this FolderSkeleton me, string foldername){
            if(FolderSkeleton.subFolders.Contains(foldername)){
                return true;
            }else{
                return false;
            }

        }
    }



}