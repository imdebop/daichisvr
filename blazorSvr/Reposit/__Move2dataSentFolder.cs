using System.IO;

namespace blazorSvr.Reposit{
    public static class Move2dataSentFolder{
        public static void Move(string folderPath){
            string destFolderPath = EnvCommon.path2mailDataSent();
            Directory.Move(folderPath, destFolderPath);         
        }
    }
}