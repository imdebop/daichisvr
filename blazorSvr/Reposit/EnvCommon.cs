using blazorSvr.Utils;
using System;

namespace blazorSvr.Reposit
{
    public class EnvCommon
    {
       public static string test(){
           return "wawawa";
       } 

       public static string path2mailFolder(){
           string mpath = LocalPaths.GetPath("mailFolder");
           return mpath;
           //return "/home/dh/yagyu/00mail/";
       }

       ///<summary>未送信のメールのタイトル、本文を保存</summary>
       public static string path2mailTextFolder(){
           string path = LocalPaths.GetPath("mailTextFolder");
           return path;
       }
       
       ///<summary>帳票更新メールの本文のテンプレートフォルダ</summary>
       public static string path2mailKoshinTemplate(){
           return LocalPaths.GetPath("mailKoshinTemplate");
       }

       public static string path2mailZipSent(){
           return LocalPaths.GetPath("mailZipSent");
       }
       
       public static string path2mailDataSent(){
           return LocalPaths.GetPath("mailDataSent");
       }
    }
}