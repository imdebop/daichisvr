using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using blazorSvr.Services;
using blazorSvr.Utils;

namespace blazorSvr.Reposit{
    public class SetUpdateFolders{
       static readonly string jobId = "帳票更新大地";

         ///<summary>更新用フォルダーを作り、親フォルダーパスを返す</summary>
        public static string AddUpdateFolder(MailState mState){
            //string[] wk = DateTime.Now.ToString().Split(" ")[0..2]; 
            //string date = wk[0].Replace("/","");
            string date = Utils.Tools.getDateOnlyNumerics();
            int n = checkUpdateExists(date);
            n++;
            string suffix = n.ToString("D2");
            string parentFolderName = date + jobId + suffix;
            mState.MailDataFolderName = parentFolderName;
            return createUpdateFolder(parentFolderName);
        }

        /// <summary>返り値は現在の最大suffix</summary>
        static int checkUpdateExists(string date){
            int suffMax = 0;
            string mailDataSentPath = EnvCommon.path2mailDataSent();
            string[] folders = Directory.GetDirectories(mailDataSentPath);
            if(folders.Count() == 0){
                return 0;
            }else{
                int num;
                //Regex rgx = new Regex(@"帳票更新大地(\d\d)$");
                foreach(string s in folders){
                    string pattern = date + jobId + @"(\d\d)$";
                    Match m = Regex.Match(s, pattern);
                    if(!m.Success){continue;}
                    string v = m.Groups[1].Value;
                    num = int.Parse(v);
                    if(num > suffMax) suffMax = num;
                }
                return suffMax;
            }
        }

        static string createUpdateFolder(string parentFolder){
            string mailFolderPath = EnvCommon.path2mailFolder();
            string parentPath = Path.Combine(mailFolderPath, parentFolder);
            if(Directory.Exists(parentPath)){
                return "Error:directory_exists!";
            }else{
                string subPath;
                Directory.CreateDirectory(parentPath);
                foreach(string sub in FolderSkeleton.subFolders){
                    subPath = Path.Combine(parentPath, sub);
                    Directory.CreateDirectory(subPath);
                }
                return parentPath;
            }
        }
    }

}